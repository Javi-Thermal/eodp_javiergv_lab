
# LEVEL-1B MODULE

from l1b.src.initL1b import initL1b
from common.io.writeToa import writeToa, readToa
from common.src.auxFunc import getIndexBand
from common.io.readFactor import readFactor, EQ_MULT, EQ_ADD, NC_EXT
from config.l1bConfig import l1bConfig
import numpy as np
import os
import matplotlib.pyplot as plt

class l1b(initL1b):

    def __init__(self, auxdir, indir, outdir):
        super().__init__(auxdir, indir, outdir)

    def processModule(self):

        self.logger.info("Start of the L1B Processing Module")

        for band in self.globalConfig.bands:

            self.logger.info("Start of BAND " + band)

            # Read TOA - output of the ISM in Digital Numbers
            # -------------------------------------------------------------------------------
            toa = readToa(self.indir, self.globalConfig.ism_toa + band + '.nc')

            # Equalization (radiometric correction)
            # -------------------------------------------------------------------------------
            if self.l1bConfig.do_equalization:
                self.logger.info("EODP-ALG-L1B-1010: Radiometric Correction (equalization)")

                # Read the multiplicative and additive factors from auxiliary/equalization/
                eq_mult = readFactor(os.path.join(self.auxdir,self.l1bConfig.eq_mult+band+NC_EXT),EQ_MULT)
                eq_add = readFactor(os.path.join(self.auxdir,self.l1bConfig.eq_add+band+NC_EXT),EQ_ADD)

                # Do the equalization and save to file
                toa = self.equalization(toa, eq_add, eq_mult)
                writeToa(self.outdir, self.globalConfig.l1b_toa_eq + band, toa)

            # Restitution (absolute radiometric gain)
            # -------------------------------------------------------------------------------
            self.logger.info("EODP-ALG-L1B-1020: Absolute radiometric gain application (restoration)")
            toa = self.restoration(toa, self.l1bConfig.gain[getIndexBand(band)])

            # Write output TOA
            # -------------------------------------------------------------------------------
            writeToa(self.outdir, self.globalConfig.l1b_toa + band, toa)

            # Plot the toas

            self.plotL1bToa(toa, band)

            self.logger.info("End of BAND " + band)

        self.logger.info("End of the L1B Module!")

    def equalization(self, toa, eq_add, eq_mult):
        """
        Equlization. Apply an offset and a gain.
        :param toa: TOA in DN
        :param eq_add: Offset in DN
        :param eq_mult: Gain factor, adimensional
        :return: TOA in DN, equalized
        """
        toa_out=(toa-eq_add)/eq_mult
        return toa_out

    def restoration(self,toa,gain):
        """
        Absolute Radiometric Gain - restore back to radiances
        :param toa: TOA in DN
        :param gain: gain in [rad/DN]
        :return: TOA in radiances [mW/sr/m2]
        """
        # self.logger.debug('Sanity check. TOA in radiances after gain application ' + str(toa_eq[1,-1]) + ' [mW/m2/sr]')
        toa_out=toa*gain

        return toa_out


    def plotL1bToa(self,toa,band):

        toa_isrf = readToa('/home/luss/my_shared_folder/ism_out/', 'ism_toa_isrf_' + band + '.nc')
        #
        fig = plt.figure(figsize=(20,10))
        n=int(len(toa)/2)

        # toa after isrf
        toa_isrf = toa_isrf[n,:]
        plt.plot(toa_isrf,label="toa isrf") # '.',color=clr, markersize=10)
        #
        # toa restored
        toa_l1b = toa[n,:]
        plt.plot(toa_l1b,label="toa restored and equalized")
        #
        # toa= toa[n,:]
        # plt.plot(toa,label="toa")
        #
        # toa not equalized
        toa_noeq= readToa('/home/luss/my_shared_folder/test1_l1b/', 'l1b_toa_noeq_' + band + '.nc')
        toa_noeq = toa_noeq[n,:]
        plt.plot(toa_noeq,label="toa not equalized")
        plt.title("Equalization and restoration of " + band)
        plt.xlabel("ACT [Pixel]")
        plt.ylabel("TOA [mW/sr/m2]")
        plt.grid()
        plt.legend()
        # plt.show()
        plt.savefig(self.outdir + "l1b_toa_all-" + band + '.png')
        plt.close(fig)
