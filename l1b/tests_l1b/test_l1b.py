
#Check for all bands that the differences with respect to the output TOA (l1b_toa_) are <0.01% for at least 3-sigma of the points.


import unittest
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from config.globalConfig import globalConfig
from common.io.writeToa import readToa

# Directory - this is the common directory for the execution of the E2E, all modules
auxdir = '/auxiliary'
indir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1B/input'
expdir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1B/output/'
outdir = '/home/luss/my_shared_folder/test1_l1b/'

globalConfig = globalConfig()


class testl1b(unittest.TestCase):

    def test_4sigmadiff(self):
        for band in globalConfig.bands:
            # Load expected and obtained outputs

            toa_exp_eq = readToa(expdir, globalConfig.l1b_toa_eq+ band + '.nc')
            toa_eq = readToa(outdir, globalConfig.l1b_toa_eq + band + '.nc')


            # Compute difference
            #toa_diff = np.divide(abs(toa_exp_eq - toa_eq), toa_exp_eq)
            #idx=np.argwhere(toa_diff==np.NaN)

            toa_diff=np.zeros((toa_eq.shape[0],toa_eq.shape[1]))
            for i in range(toa_diff.shape[0]):
                for j in range(toa_diff.shape[1]):
                    if toa_exp_eq[i,j]==0:
                        toa_diff[i,j] = 0
                    else:
                        toa_diff[i,j]=np.divide(abs(toa_exp_eq[i,j] - toa_eq[i,j]), toa_exp_eq[i,j])


            #x=np.isnan(toa_diff)
            #y=np.argwhere(x==true)


            # Assert specifications
            self.assertTrue(3 * np.std(toa_diff) < 0.01 / 100)


if __name__ == '__main__':
    unittest.main()
