
# MAIN FUNCTION TO CALL THE L1B MODULE
import matplotlib.pyplot as plt

from l1b.src.l1b import l1b

# Directory - this is the common directory for the execution of the E2E, all modules
auxdir = '/home/luss/EODP/EODP_JavierGV/eodp_javiergv_lab/auxiliary/'
indir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1B/input/'
outdir = '/home/luss/my_shared_folder/test1_l1b/'

# Initialise the ISM
myL1b = l1b(auxdir, indir, outdir)
myL1b.processModule()


#fig=plt.figure(figsize=(20,10))
#plt.plot(toa_l1b, outputdir, band)

#plt.show()
